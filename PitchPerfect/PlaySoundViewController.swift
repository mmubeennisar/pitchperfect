//
//  PlaySoundViewController.swift
//  PitchPerfect
//
//  Created by Mubeen Nisar on 3/23/16.
//  Copyright © 2016 Sleek Solutions. All rights reserved.
//

import UIKit
import AVFoundation

class PlaySoundViewController: UIViewController {
    var recordedAudio:NSURL!
    var audioFile:AVAudioFile!
    var audioEngine:AVAudioEngine!
    var audioPlayerNode:AVAudioPlayerNode!
    var stopTimer:NSTimer!
    var audioTime:NSTimeInterval!
    var audioPlayData:AVAudioPlayer!
    
    @IBOutlet weak var snailButton: UIButton!
    @IBOutlet weak var rabbitButton: UIButton!
    @IBOutlet weak var chipmunkButton: UIButton!
    @IBOutlet weak var darthvaderButton: UIButton!
    @IBOutlet weak var parrotButton: UIButton!
    @IBOutlet weak var echoButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var durationLabel: UILabel!
    enum ButtonType:Int{
        case Snail=0,Rabbit,Chipmunk,Darthvader,Parrot,Reverb
    }
    
    @IBAction func playSoundForbutton(sender:UIButton){
        switch(ButtonType(rawValue: sender.tag)!){
        case.Snail:
            playSound(rate: 0.5)
        case.Rabbit:
            playSound(rate: 1.5)
        case.Chipmunk:
            playSound(pitch:1000)
        case.Darthvader:
            playSound(pitch:-1000)
        case.Parrot:
            playSound(reverb:true)
        case.Reverb:
            playSound(echo:true)
        }
        configureUI(.Playing)
    }
    @IBAction func stopButtonPressed(sender:AnyObject){
        
        stopAudio()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAudio()
        setDurationLabel()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        
        configureUI(.NotPlaying)
    }
    func setDurationLabel(){
        audioPlayData=try! AVAudioPlayer(contentsOfURL:recordedAudio)
        let durationString:String = String(format:"%.1f", audioPlayData.duration)
        durationLabel.text="Duration: \(durationString) Seconds"
        
    }


}
