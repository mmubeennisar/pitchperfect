//
//  ViewController.swift
//  PitchPerfect
//
//  Created by Mubeen Nisar on 3/20/16.
//  Copyright © 2016 Sleek Solutions. All rights reserved.
//

import UIKit
import AVFoundation

class RecordSoundViewController: UIViewController,AVAudioRecorderDelegate {
    var audioRecorder:AVAudioRecorder!
    @IBOutlet weak var recordingLabel: UILabel!
    @IBOutlet weak var endRecord: UIButton!
    @IBOutlet weak var startRecord: UIButton!
    

    

    @IBAction func recordAudio(sender: AnyObject) {
        endRecord.enabled=true
        startRecord.enabled=false
        recordingLabel.text="Recording in Progress"
        let dirPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)[0] as String
        
        let recordingName = "recordedVoice.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = NSURL.fileURLWithPathComponents(pathArray)
        
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayAndRecord)
        
        try! audioRecorder = AVAudioRecorder(URL: filePath!, settings: [:])
        audioRecorder.delegate=self
        audioRecorder.meteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
    }
    
    @IBAction func stopRecording(sender: AnyObject) {
        endRecord.enabled=false
        startRecord.enabled=true
        recordingLabel.text="Tap To Record"
        audioRecorder.stop()
        let audioSession=AVAudioSession.sharedInstance()
        try! audioSession.setActive(false)
    }
    override func viewWillAppear(animated: Bool) {
        endRecord.enabled=false
    }
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder, successfully flag: Bool) {
        if(flag){
            performSegueWithIdentifier("stopRecording", sender: audioRecorder.url)
            
        }
        else{
            showAlert()
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier=="stopRecording"){
            let playSoundVC=segue.destinationViewController as!
            PlaySoundViewController
            let recordedAudioURL=sender as! NSURL
            playSoundVC.recordedAudio=recordedAudioURL
            
        }
    }
    func showAlert(){
        let alert = UIAlertController(title: "Alert", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

